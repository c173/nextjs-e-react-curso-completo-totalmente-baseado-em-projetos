import MenuItem from "./MenuItem"
import Logo from "./Logo"
import { IconeCasa, IconeCurso, IconeSair } from "../icons"
import useAuth from "../../data/hook/useAuth"


export default function MenuLateral() {
  const { logout } = useAuth()

  return (
    <aside className={`
           flex flex-col
           bg-gray-200 text-gray-700
           dark:bg-gray-900 dark:text-gray-200
           `}>
      <div className={`
           flex flex-col items-center justify-center
           bg-gradient-to-r from-indigo-500 to-purple-800
           h-20 w-20
      `}>
        <Logo />
      </div>
      <ul className="flex-grow">
        <MenuItem url="/" text="Home" icone={IconeCasa} />
        <MenuItem url="/cursos" text="Cursos" icone={IconeCurso} />
        <MenuItem url="/" text="Home" icone={IconeCasa} />
      </ul>
      <ul>
        <MenuItem
          text="Sair"
          icone={IconeSair}
          onClick={logout}
          className={`
              text-red-600 dark:text-red-400
              hover:bg-red-400 hover:text-white
              dark:hover:text-white
          `} />
      </ul>
    </aside>
  )
}