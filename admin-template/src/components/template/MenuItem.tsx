import Link from 'next/link'
import { checkPropTypes } from 'prop-types'

interface MenuItemProps {
  url?: string
  text: string
  icone: any
  className?: string
  onClick?: (evento: any) => void //Aqui o parametro recebe uma função 
}

export default function MenuItem(props: MenuItemProps) {
  function renderizarLink() {
    return (
      <a className={`
           flex flex-col justify-center items-center
           h-20 w-20 
           dark:text-gray-200
           ${props.className}
        `}>
        {props.icone}
        <span className={`
                text-xs font-light 
          `}>
          {props.text}
        </span>
      </a>
    )
  }
  return (
    <li onClick={props.onClick} className={`
    hover:bg-gray-300  dark:hover:bg-gray-800
      cursor-pointer
    `}>
      {props.url ? (
        <Link href={props.url}>
          {renderizarLink()}
        </Link>
      ) : (
        renderizarLink()
      )}
    </li>
  )
}