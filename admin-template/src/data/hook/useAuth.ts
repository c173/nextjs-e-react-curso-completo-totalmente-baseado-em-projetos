import { useContext } from "react";
import AuthContext from "../context/AuthContext";

//TODO hook criado para ter acesso aos dados providos pelo contexto ( autenticação )
const useAuth = () => useContext(AuthContext)

export default useAuth