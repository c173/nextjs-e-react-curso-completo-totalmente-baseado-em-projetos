import { useContext } from "react";
import AppContext from "../context/AppContext";

//TODO hook criado para ter acesso aos dados providos pelo contexto
const useAppData = () => useContext(AppContext)

export default useAppData